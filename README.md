# Curium Software 

The Curium Bus contains three CPUs on the following subsystems:
- OBC / main / EPS / ADCS: STM32H743
- Satnogs-Comms MCU: STM32H723
- Satnogs-Comms FPGA: Xilinx Zynq based System on a Module

The Satnogs-Comms software development can be followed at the [repective gitlab repository](https://gitlab.com/librespacefoundation/satnogs-comms).

As Satnogs-Comms MCU the Curium OBC runs Zephyr. To get started with development following steps can be taken:
- Follow the [Zephyr Getting Started Guide](https://docs.zephyrproject.org/latest/develop/getting_started/index.html)
- Get a STlink programmer (Contained on several STM-Nucleo boards)
- Get Harwin M-80 or Tag connect cables according to the used connectors in the [OBC repository](https://gitlab.com/curiumsat/pcbs/curium2_main)
- Make sure to understand the basics of Zephyr development and its hardware abstracion: [link](https://docs.zephyrproject.org/latest/hardware/porting/board_porting.html)

The Main SW has to address the following system:
![i2c_overview](image.png)